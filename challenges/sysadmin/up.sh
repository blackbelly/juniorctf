 #!/bin/bash

docker run -d -p 2022:22 --name john1 juniorctf/john1
# docker run -d -p 2122:22 --name john2 juniorctf/john2
# docker run -d -p 2222:22 --name boris juniorctf/boris
docker run -d -p 2322:22 --name unix1 juniorctf/unix1
docker run -d -p 2422:22 --name exfiltration1 juniorctf/exfiltration
docker run -d -p 2423:22 --name exfiltration2 juniorctf/exfiltration
docker run -d -p 2424:22 --name exfiltration3 juniorctf/exfiltration
docker run -d -p 2425:22 --name exfiltration4 juniorctf/exfiltration
docker run -d -p 2426:22 --name exfiltration5 juniorctf/exfiltration
docker run -d -p 2427:22 --name exfiltration6 juniorctf/exfiltration
docker run -d -p 2522:22 --name leiter juniorctf/leiter
# docker run -d -p 2622:22 --name john4 juniorctf/john4
# docker run -d -p 2722:22 --name john5 juniorctf/john5
# docker run -d -p 2822:22 --name john6 juniorctf/john6
docker run -d -p 2922:22 --name log1 juniorctf/log1
docker run -d -p 2932:22 --name hydra1 juniorctf/hydra1
# docker run -d -p 2942:22 --name hydra2 juniorctf/hydra2
# docker run -d -p 8080:80 --name net1 juniorctf/net1
