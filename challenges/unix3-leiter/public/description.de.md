# Leiter

James, England braucht sie! Wir haben herausgefunden, dass eine planetare Katastrophe droht.
Felix Leiter führt die Operation an, aber er ist verschwunden ...
Es eilt!

Sie werden auf den Rechner von Felix zugreifen und versuchen, die fehlenden Informationen zu finden.

Ich hoffe, sie haben ihre Unix-Lektionen gut gelernt! Falls nicht, gehen sie zurück zu *Golden Eye* oder *Exfiltration*.

Viel Glück, James!

## Zugriff auf den Rechner von Felix

Benutze SSH:

- port `2522`
- Benutzername: `leiter`
- Passwort: `burger4ever`
- Rechner: `192.168.0.8`
