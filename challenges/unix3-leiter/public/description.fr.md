# Leiter

James, l'Angleterre a besoin de vous ! Nous avons appris qu'une catastrophe planétaire risquait de se produire.
Félix Leiter dirigeait l'opération, mais il est introuvable... 
Il faut faire vite !

Vous allez accéder à l'ordinateur de Félix pour essayer de trouver les informations qui nous manquent.

J'espère que vous avez bien suivi (et retenu) vos leçons d'Unix ! Sinon, retour à *Golden Eye* ou *Exfiltration*.

Bonne chance, James !



## Accéder à l'ordinateur de Félix

Utilise SSH:

- port `2522`
- utilisateur: `leiter`
- mot de passe: `burger4ever`
- ordinateur: `192.168.0.8`
