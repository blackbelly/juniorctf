# John 1

Palpaguin hat gerade ein Kontrollsystem für den Schwarzen Stern entwickelt.

Rebellen haben es geschafft (auf Kosten ihres Lebens), Zugang zu
diesem System zu erhalten. Sie hatten Zeit, eine wichtige Datei zu
kopieren, die Palpaguins Hash-Passwort enthielt. Sie hatten jedoch
keine Zeit, genau das Passwort zu finden.

Sie wissen nur, dass das Palpaguin-Passwort eine *Frucht* ist.

Kannst du uns helfen, uns auf dem Rechner anzumelden?
Du hast die gehashte Passwortdatei (zum Download).

## Zugriff auf das Steuerungssystem

Um auf das Black Star-Steuerungssystem zuzugreifen, geben Sie Folgendes ein: `ssh -p 2022 palpaguin @ 192.168.1.9`

Erklärung:

- ssh ist ein wichtiges Protokoll, das wir sehr oft verwenden.
- `-p 2022` gibt den zu verwendenden *Port* der Verbindung an. Das ist
  wie die Nummer einer Tür, aber an einem Computer.
- `palpaguin` ist der Palpaguin-Benutzername
- "192.168.1.9" ist die Serveradresse


Wenn du so eine Meldung bekommst, antworte mit "ja" ("yes"):
```
The authenticity of host '[127.0.0.1]:2022 ([127.0.0.1]:2022)' can't be established.
RSA key fingerprint is 2d:93:f1:87:f8:91:8f:87:46:63:72:fd:c9:24:ed:d9.
Are you sure you want to continue connecting (yes/no)?
```

Dann fragt dich das System nach einem Passwort. Dies ist das Passwort,
das du herausfinden sollst.

```
palpaguin@192.168.1.9's password: 
```

## Achtung

Dies ist ein Spiel. Im wirklichen Leben ist es verboten, Passwörter zu
knacken. Du darfst das niemals ohne Erlaubnis tun (auch nicht zum
Spass oder um dein Publikum zu beeindrucken)!.

Manchmal kann es aber nötig sein, wenn es um *den eigenen Computer*
geht und du das Passwort vergessen hast. Auf deinen eigenen Rechner
darft du selbstvertändlich zugreifen!

## Passwörter cracken mit John


1. Installiere das Tool *John The Ripper*, indem du Folgendes in ein
   Terminal eingibst: `sudo apt-get install john`

2. Schreibe eine Liste der Früchte, die du kennst, in eine
   Datei. Eine Frucht pro Zeile. Speicher diese Datei.

3. John The Ripper kann mögliche Passwörter für dich testen. Führe
   dazu in einem Terminal den Befehl `sudo john --wordlist=dein_früchte_file mdphache`.

