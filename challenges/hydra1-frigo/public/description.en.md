# Pico's Fridge

[Pico The Croc'](http://picolecroco.free.fr) really cares for his champaign.
He keeps it cold and secure in his fridge.
But we're just back from a looong hike (15 miles), and we're thirsty. Very thirsty. We'd definetely like to have a drink, but ... Pico has protected his fridge with a password !! Can you believe it? Protecting a fridge with a password. Well anyway he's done it.

Your mission: open the fridge and drink Pico's champaign.

## Open the door of the fridge

You access the door via SSH. Remember how to do that?


```
- Name of the fridge: ADRESSE IP
- Port: 2932
- Identifier: pico
```

Now, find the password. That's your job!


## Reminders for real life

- When you're thirsty, water is far more efficient ;)
- It is **forbidden** to access hosts if you haven't been authorized to. Really.


## Hint

A hint for the password? No! Do you think you'll get hints in the real life? No.


## Brute force

To solve this challenge, I advise you use what is called "brute force". It doesn't mean you need to be a brute ;-)

You could try out plenty of passwords to open the fridge. Eventually, you'd get the right one. Unless you're really lucky, this is going to take you lots of time.

So, we'll be lazy and ask a program to do the job for you. The program will test hundreds of passwords for you. This program is called **Hydra**.

## Hydra

Install **Hydra**  on your host:

```
sudo apt-get install hydra hydra-gtk
```

Let's have a closer look at Hydra.


## Manual

Nearly all Unix commands come with "manual pages". That's short and ugly documentation explaining how to use the command or program.

Try it:

```
man hydra
```

**man** is the Unix command to show a manual page. Then, type *hydra* to access the documentation of Hydra. **It's important you try this whenever you need some help on a command on Unix**.

Manual pages come with a short description:


```
HYDRA(1)                    General Commands Manual                   HYDRA(1)

NAME
       hydra  - a very fast network logon cracker which support many different
       services
```

Then, it explains all possible options.


```
SYNOPSIS
       hydra
        [[[-l LOGIN|-L FILE] [-p PASS|-P FILE|-x OPT]] | [-C FILE]] [-e nsr]
        [-u] [-f] [-F] [-M FILE] [-o FILE] [-t TASKS] [-w TIME] [-W TIME]
        [-s PORT] [-S] [-4/6] [-vV] [-d]
        server service [OPTIONAL_SERVICE_PARAMETER]
```

- When you see [...] it means this is an *option* (so, it's not mandatory)
- If the [] are not there, the parameter is *mandatory*
- Keep the same order for parameters

For instance, you see here that hydra will always be followed by a *server* and a *service*. You may also specify the login name "-l LOGIN", but that's optional.

Then the manual explains each argument one by one.

## Let's roll out!

We want to try hundreds of passwords on Pico's fridge. This means you've got to run Hydra with:

- a service. In your case, this is **ssh**
- a server. This will be **IPADDRESS**
- a login. That's the user you try to get logged in. This is **pico**.
- passwords to test. I am providing the list of the 500 most frequent passwords. Download it and use it with Hydra.

Your command will look like:

```bash
$ hydra -l pico -P ./500-worst-passwords.txt ADRESSE ssh
```

Actually, it's *not exactly that*. You'll need to find the option to specify the **port number** which is 2932. But I'm sure you can do that!

## If ever

With Hydra, if you get this error:

```
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
```

log in manually to Pico's fridge, and then try again automatically with Hydra.

