# Der Kühlschrank von Pico

[Pico das Krokodil](http://picolecroco.free.fr) liebt seinen
Champagner sehr. Er bewahrt ihn in seinem Kühlschrank auf. Nur, das
ist das Problem: Wir haben gerade eine lange Wanderung gemacht (20
km), und wir sind sehr durstig. Wir würden gerne etwas trinken. Aber
... arg ... Pico hat seinen Kühlschrank mit einem Passwort geschützt!
Kann man's glauben? Na dann !!!

Deine Mission: Öffne den Kühlschrank, um den Champagner von Pico zu trinken.

## Öffne die Kühlschranktür

Du kannst die Kühlschranktür von Pico per SSH erreichen. Erinnerst du dich, wie es geht?

```
- Name des Kühlschranks: 192.168.1.9
- Port: 2932
- Username: pico
```

Es liegt an dir, das Passwort zu finden!

## Erinnerungen für das echte Leben

- Wenn du durstig bist, ist es besser, Wasser zu trinken ;)
- Es ist **verboten**, auf fremde Maschinen zuzugreifen, wenn du dafür keine Erlaubnis hast. Wirklich wahr.

## Hinweis

Einen Hinweis auf das Passwort? Nein! Im "wirklichen Leben" wirst du nicht unbedingt Hinweise haben!

## Rohe Gewalt

Um dieses Rätsel zu lösen, rate ich dir, die "Brute Force" - Methode
(auf Englisch) zu verwenden. Nein, nein, das bedeutet nicht wirklich,
dass du "rohe Gewalt" anwenden sollst, nur im übertragenen Sinn.

Du kannst einfach eine Menge Passwörter ausprobieren, um die
Kühlschranktür zu öffnen. Bis du das richtige findest. Wenn du kein
unverschämtes Glück hast, kann das sehr lange dauern.

Wieder einmal werden wir etwas faul sein, und ein Programm die Arbeit
für dich erledigen lassen. Es wird für dich Hunderte von Passwörtern
ausprobieren. Dieses Programm heißt **Hydra**.

## Hydra

Installiere **Hydra** auf deiner Maschine:

```
sudo apt-get install hydra hydra-gtk
```

Schauen wir uns an, was Hydra macht ...


## Handbuch

Fast alle Unix-Befehle und -Programme haben "manual pages" (auf
deutsch: Handbuchseiten). Das ist eine prägnante und überhaupt nicht
poetische Anleitung zum Benutzen des Befehls / Programms.

Probiere mal:

```
man hydra
```

**man** ist der Unix-Befehl zum Anzeigen einer Handbuchseite. Du
  tippst danach *hydra*, um die Dokumentation für Hydra zu
  bekommen. **Das sollte ein automatischer Reflex** für dich werden,
  wenn du nicht weißt, wie ein Befehl, ein Programm, oder eine
  Unix-Funktion funktioniert.

Alle Handbuchseiten beginnen ganz oben mit einer kurzen Beschreibung der Sache:


```
HYDRA(1)                    General Commands Manual                   HYDRA(1)

NAME
       hydra  - a very fast network logon cracker which support many different
       services
```

Dann kommt etwas, das dir die möglichen Optionen erklärt.


```
       hydra
        [[[-l LOGIN|-L FILE] [-p PASS|-P FILE|-x OPT]] | [-C FILE]] [-e nsr]
        [-u] [-f] [-F] [-M FILE] [-o FILE] [-t TASKS] [-w TIME] [-W TIME]
        [-s PORT] [-S] [-4/6] [-vV] [-d]
        server service [OPTIONAL_SERVICE_PARAMETER]
```

- Wenn um eine Option Klammern stehen, ist sie *optional*.
- Sonst muss sie da sein.
- Die Reihenfolge der Parameter muss normalerweise eingehalten werden.

Zum Beispiel siehst du hier, dass Hydra immer einen *server* und einen
*service* haben will, während zum Beispiel der Login-Name "-l
LOGIN" *optional* ist.

Schließlich erklärt der Rest des Handbuchs jeden Parameter einzeln.

## Ich möchte loslegen!

Wir wollen viele Passwörtern an Picos Kühlschrank ausprobieren. Das bedeutet, dass du Hydra aufrufen mußt mit:

- der Service, das ist **ssh**
- Der Server, das ist **192.168.1.9**.
- Der Login ist der Benutzer, als der man sich anmelden möchte, also **pico**.
- Passwörter zum Testen: Ich gebe dir eine Liste der 500 häufigsten Passwörter. Lade sie herunter und benutze sie mit Hydra.

Dein Kommando sieht also etwa so aus:


```bash
$ hydra -l pico -P ./500-worst-passwords.txt 192.168.1.9 ssh
```

In Wirklichkeit ist es das *nicht ganz*. Du brauchst noch eine Option,
um die **Portnummer** anzugeben, die **2932** ist. Ich lasse dich mal
suchen!

## Nur für den Fall

Wenn mit Hydra eine Menge dieser Meldungen auftauchen:


```
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
[ERROR] ssh protocol error
```

Versuche, dich ein erstes Mal manuell in den Kühlschrank von Pico
einzuloggen, und versuche es dann erneut mit Hydra.
