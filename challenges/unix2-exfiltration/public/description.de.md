# Exfiltration

James, hören sie gut zu! Auf dem Computer von Boris Grishenko liegen die Pläne von Golden Eye.
Ich möchte, dass sie mir diese Pläne bringen, damit Großbritannien diese Geheimwaffe untersuchen kann.

Sie werden die Datei mit den Geheimplänen in das Verzeichnis `/mi6`
kopieren.  Dann führen sie in einem Terminal folgenden Befehl aus, um
zu überprüfen, ob sie das richtige File gefunden haben:

`(ls /mi6 && cat /mi6/geheimplan) | sha1sum`

Sie werden eine Ausgabe erhalten, die etwa so aussieht:

```
45c9646a8342d48f04b4222a604aaaaaaaaa3863 -
```

Die Flagge ist also `45c9646a8342d48f04b4222a604aaaaaaaaa3863`.

Natürlich ist dies hier nicht die echte Flagge, die müssen sie schon selbst finden, James!
An die Arbeit!

## Der Computer von Boris

Du kannst dich via SSH in den Computer von Boris einloggen:

- port `2422` (ironman), `2423` (asterix), `2424` (zorro), `2425` (ola), `2426` (dio), `2427` (avatar)
- username: `boris`
- password: `computer`
- host: `192.168.1.9`


## Einige nützliche Befehle 

- Ein File kopieren: `cp ORIGINAL_FILE ZIEL`
- Ein File löschen: `rm FILE`. 
- Ein Verzeichnis kopieren: `cp -R ORIGINAL_VERZEICHNIS ZIEL`
- Ein Verzeichnis erstellen: `mkdir VERZEICHNIS`
- Ein Verzeichnis löschen: `rmdir VERZEICHNIS`. Das funktioniert nicht, wenn das Verzeichnis nicht leer ist.

Vergiss die Geschichte mit den absoluten und relativen Pfaden nicht:
Du kannst die Namen von Dateien oder Verzeichnissen als absoluten oder
als relativen Pfad eingeben... Erinnere dich an die Aufgabe "Unix-1 /
Golden Eye".

## Versteckte Dateien und Verzeichnisse

Hallo James, hier spricht Q !

Wenn man unter Unix den Befehl `ls` ausführt, sieht man die Dateien
und Verzeichnisse **nicht*, deren Name mit einem `.` beginnt.  Wenn
ein File zum Beispiel `.dingsda` heißt, sieht man es nicht.

Um es zu sehen, muss man dem Befehl `ls` einen Parameter geben und der
heißt `-a`. Also, mit `ls -a` ("a" wie **a**lle) kannst du alle
Dateien in einem Verzeichnis sehen.

Ich sage das nur so ....
