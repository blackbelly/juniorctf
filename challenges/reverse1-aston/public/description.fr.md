# L'Aston Martin de Pico

*Il faut résoudre l'énigme des voitures de Pico avant d'aborder cette énigme*

As-tu réussi à emprunter l'Aston Martin de Pico ? A priori non...

Pique lui son Aston Martin !

## Entre dans le garage de Pico

Tu y accèdes via SSH:

- port: `2822`
- utilisateur: `pico`
- machine: `ADRESSE-IP-MACHINE`
- mot de passe de Pico: tu as trouvé ça dans l'énigme du garage de Pico

## Fouille le garage de Pico

Tu devrais trouver un fichier qui s'appelle `astonkeys` quelque part. Regarde bien.

## Astonkeys

Ce fichier est un programme. On l'appelle aussi un **executable**.
Lance le:

```
./astonkeys
```

Qu'est-ce que ca te dit ?

## Reverse 

Bon, tu n'as pas encore les clés. Elles sont juste cachées dans `astonkeys`.

Pour les trouver, lance : `strings astonkeys` et lit bien **tout** ce qui s'affiche !

## Emprunter l'Aston Martin

Tu y accedes via SSH:

- port: `2822`
- utilisateur:  tu as trouve le nom d'utilisateur dans l'enigme des voitures de Pico
- machine: `ADRESSE IP MACHINE ICI`
- mot de passe pour l'Aston Martin: tu viens de le trouver
