# Mission (Im)possible

Ethan, wir haben erfahren, dass der "Gute Doktor" wieder eine
gefährliche Aktion gegen unser Land plant, besonders gegen unsere
Sektion, den IMF (Impossible Missions Force).


Wir haben die Logindaten des "Guten Doktor" herausbekommen. Es liegt an dir, herauszufinden was er vor hat ....

## Zugang zur Maschine des "Guten Doktor"

- IP-Adresse: 192.168.1.9
- port: 2922
- Username: `doktorgut`
- Password: `synd1cate`


## History, ein Keylogger, ohne programmieren zu müssen ...

Ich denke es ist nicht nötig, dich daran zu erinnern, Ethan, dass
unter Unix alles, was du in deiner Shell eingibst, in einer *history*
(Englisch für "Geschichte") gespeichert wird. Diese *history* kann man
mit dem Kommando `history` abrufen.

Beispiel:

```
$ history
...
  539  make build
  540  make build
  541  make build
  542  make run
```

Das ist sehr praktisch, weil man ein ausgeführtes Kommando wieder
aufrufen kann, indem man `!num` eingibt, wobei `num`durch die Zahl in
der *history* ersetzt wird. Im Beispiel oben, wenn man `!542` eingibt,
wird `make run` ausgeführt. Das ist besonders nützlich für lange
Kommandos.
