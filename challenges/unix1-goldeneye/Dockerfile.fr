FROM alpine:3.7

ENV USER boris
ENV USER_PASSWORD computer

RUN apk update && apk upgrade && apk add bash openssh &&  rm -rf /var/cache/apk/* /tmp/*

RUN adduser -D $USER
RUN echo "${USER}:${USER_PASSWORD}" | chpasswd

# make sure we get fresh keys
RUN rm -rf /etc/ssh/ssh_host_rsa_key

ADD docker-entrypoint.sh /

# create files
RUN echo "Va dans le répertoire severnaya" > /etc/motd
RUN echo "================ CLES de LANCEMENT ===========" > /etc/cles-lancement
RUN echo "La deuxième partie du code est: NeverCatchThePlane" >> /etc/cles-lancement
RUN echo "Colle les deux parties pour reconstituer le drapeau et pour marquer les points" >> /etc/cles-lancement


RUN mkdir -p /home/$USER/severnaya/goldeneye
RUN echo "Rien d'important ici. C'est juste pour faire illusion :)" > /home/$USER/blabla
RUN echo "Les plans secrets de Golden Eye se trouvent dans le sous répertoire goldeneye" > /home/$USER/severnaya/infos.txt
RUN echo "Pour déclencher golden eye, il faut deux parties de drapeau" >  /home/$USER/severnaya/instructions-lancement
RUN echo "============== PLANS SECRETS ======================" >  /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "GoldenEye est une arme redoutaaaaaable. Pour éviter qu'elle ne soit utilisée par un fou furieux, il faut deux codes." >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "La première partie de ce code est: JamesBondWill" >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "La deuxième partie du code se trouve dans les clés de lancement." >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "  ooo,    .---. " >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo " o   o   /    |\________________" >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "o     oooo()  | ________   _   _)" >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo " oo   o  \    |/        | | | |" >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "   ooo    ---            -  |_|" >> /home/$USER/severnaya/goldeneye/plans-secrets
RUN echo "Les clés de lancement sont dans le fichier ../../../../etc/cles-lancement" >>  /home/$USER/severnaya/goldeneye/plans-secrets
RUN chown -R boris /home/$USER



EXPOSE 22
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/sbin/sshd", "-D" ]
