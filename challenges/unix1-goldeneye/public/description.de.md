# Einführung: Golden Eye

Das Leben von Boris Grishenko beschränkt sich auf seinen Computer, auf
dem alle Informationen gespeichert sind, die er sich merken muss. Zum
Glück konnte 007 den Laptop von Boris wiederbekommen und dem Team
von Q geben.

Du arbeitest für Q und du musst die wichtigen Informationen auf Boris'
Computer wiederfinden. Diese Informationen sind in Dateien (Englisch:
"files") auf diesem Rechner versteckt. Du hast Zugang zu Boris'
Rechner, aber du musst die Files finden und lesen, die die geheimen
Informationen enthalten.

## Zugang zu Boris' Computer

Benutze SSH:

- port `2322`
- Benutzername: `boris`
- Passwort: `computer`
- IP-Adresse: **192.168.1.9**

## Dateien ("files") und Verzeichnisse ("Englisch: "directories")

### Ja, ja, aber ich will jetzt die Aufgabe jetzt lösen!

Um die Lösung zu finden, musst du die Verzeichnisse aufsuchen und die Dateien lesen.
Sobald du verstanden hast:

- wie man von einem Verzeichnis in ein anderes wechselt, und
- wie man eine bestimmte Datei liest

... wirst du diese Rätsel lösen können

Die folgenden Abschnitte helfen dir dabei.

### Verzeichnisbaum

Unter Unix sind alle Dateien in einem Verzeichnisbaum organisiert.
Ein Verzeichnisbaum ist ein bisschen wie ein Baum. Da gibt es zunächst
den Baumstamm: Auf dem Computer heißt der "Wurzelverzeichnis"
(Englisch: "root") und das entspricht dem Verzeichnis `/`.

Direkt vom Wurzelverzeichnis (*root*) gehen große Äste ab. Das sind
die obersten Verzeichnisse, zum Beispiel `/home` oder `/etc`. Das
bedeutet "in einem Verzeichnis mit dem Namen *etc*, das vom
Wurzelverzeichnis abgeht" ("direkt unter *root* liegt", das bedeutet
alles das gleiche).

Dann gibt es noch viele kleine Zweige. Das sind Verzeichnisse
innerhalb dieser obersten Verzeichnisse. Zum Beispiel `/home/tig`. Das
bedeutet "in einem Verzeichnis mit Namen *tig*, das im Verzeichnis
*home* liegt, das direkt unterhalb von *root* liegt.

### Pfad (Englisch: "path")

Es gibt zwei Möglichkeiten, einen Pfad zu schreiben, das heisst den
Weg, den man nehmen muss, um zu einem bestimmten Verzeichnis
(eng. "directory") oder zu einer bestimmten Datei ("file") zu
gelangen.

- **absoluter Pfad**. Du beschreibst, wie man zu einem bestimmten
  Verzeichnis oder zu einer Datei **vom Wurzelverzeichnis ("root") aus**
  kommt. **Ein absoluter Pfad fängt immer mit / an.**

- **relativer Pfad**. Du beschreibst, wie man zu einem bestimmten
  Verzeichnis oder zu einer Datei **von dort, wo du dich gerade
  befindest** kommt. Wenn du also in `/home/tig` bist, und mit einem
  *relativen Pfad* nach `/etc` möchtest, musst du zuerst ins
  Verzeichnis, das `tig` enthält (das heisst `home`). Von dort musst du
  zum Wurzelverzeichnis, und dann nach `/etc`.
  **Ein relativer Pfad fängt niemals mit / an**

Unter Unix heisst das Verzeichnis, in dem du gerade bist `.`.
Das Verzeichnis `über` dir, heisst `..`.

Stell dir vor, dass das Verzeichnis von Boris so aussieht:

```
./boris/
|-------- Dockerfile
|-------- john.conf
|-------- public
|         |---- description.md
|-------- words
```

Vom Verzeicnhnis `boris` aus ist der relative Pfad zur Datei `description.md`: `./public/description.md`.
Vom Verzeicnhnis `public` aus ist der relative Pfad zur Datei `john.conf`: `../john.conf`.

Vom Verzeicnhnis `boris` aus, was ist `./././././Dockerfile` ? Denk
nach! `.` setht für das aktuelle Verzeichnis. Also wechselt man
nirgendwo hin. Am Schluss ist es einfach die Datei
`Dockerfile`. Übrigens, oft schreibt man statt `./Dockerfile` einfach
`Dockerfile` (Das bedeutet "die Datei Dockerfile im aktuellen
Verzeichnis").

Was ist der Unterschied zwischen  `./john.conf`, `john.conf` und `/john.conf`. Achte auf die Details ...

- `./john.conf` bedeutet "die Datei john.conf im aktuellen Verzeichnis".
- `john.conf` bedeutet das Gleiche.
- `/john.conf` fängt mit `/`an. Das ist ein *absoluter Pfad* Das ist
  die Datei john.conf, **die im Wurzelverzeichnis liegt**. Also ist
  das (wahrscheinlich) eine andere Datei, es sei denn, du befindest
  dich gerade im Wurzelverzeichnis...


### DAs Verzeichnis wechseln

- Um aus einem Verzeichnis heraus zu kommen, nimmt man den Befehl
  `cd`, gefolgt vom Namen des (neuen) Verzeichnisses. Du kannst
  einen **absoluten Pfad** oder einen **relativen Pfad**
  angeben.
  
  
Beispiel für einen *absoluten Pfad*: `cd /etc`
Beispiel für einen *relativen Pfad*: `cd ./etc`.

Beachte, dass diese beiden Befehle dich zu unterschiedlichen
Verzeichnissen führen können. Der zweite Befehl geht von dem Ort, an
dem du gerade bist, in ein Verzeichnis namens `etc`. Wenn du also
gerade in `/home/tig` bist, dann landest du in`/home/tig/etc`.



### Den Inhalt eines Verzeichnisses anzeigen

Das geht mit dem Befehl `ls`, gefolgt vom Namen des Verzeichnisses,
das du anschauen willst. Natürlich kannst du wieder einen absoluten
oder einen relativen Pfad angeben.

Beispiel:

```
$ ls /usr
bin    include  lib32  local  share  x86_64-linux-gnu
games  lib      lib64  sbin   src
```
Der Befehl `ls`hat sehr viele Optionen. Wir schauen uns die nicht alle an, aber es ist nützlich, wenn du `ls -lh` kennst.

Beispiel:

```
$ ls -lh
total 24K
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:53 boris
-rwxr--r-- 1 axelle axelle  116 juil.  7 22:44 down.sh
drwxr-xr-x 3 axelle axelle 4,0K juil.  6 19:32 john1
drwxr-xr-x 3 axelle axelle 4,0K juil.  7 22:26 john2
drwxr-xr-x 3 axelle axelle 4,0K juil.  8 18:26 unix1
-rwxr--r-- 1 axelle axelle  176 juil.  7 22:44 up.sh
```

Was sieht man hier?

- Bei Zeilen, die mit einem kleinen `d` anfangen, handelt es sich im ein Verzeichnis.
- Die anderen sind Dateien

Im Beispiel oben sind also boris, john1, john2 und unix1 Verzeichnisse, während down.sh und up.sh Dateien sind.

### Den Inhalt einer Datei anzeigen

Du kannst einen Editor wie `emacs` oder `bluefish` benutzen, aber es geht schneller: Du kannst den Befehl `cat` nehmen.
Natürlich kann nach dem Befehl wieder der absolute oder relative Pfad zur Datei stehen.

Beispiel:

```
$ cat ./host.conf 
# The "order" line is only used by old versions of the C library.
order hosts,bind
multi on
```

Wenn die Datei gross ist, ist es besser den Befehl `more` zu benutzen, der dir die Datei Seite für Seite anzeigt.

Wenn du den Inhalt einer Programmdatei anachaust, wirst du viele sehr seltsame Zeichen sehen, weil Computer nicht die gleiche Sprache sprechen wie wir.

Wenn du versuchst mit `cat` ein Verzeichnis anzuschauen, wird `cat` dir sagen, dass das nicht geht:

```
$ cat /etc
cat: /etc: Is a directory
```
