# Spectre contre attaque
Catégorie: Réseau
Points: 60

Bonjour Numéro 10.

Ici Numéro 1.
"Miaooouw". Et lui, c'est mon horrible chat siamois.

Si tu veux vraiment rentrer au sein de [S.P.E.C.T.R.E](https://en.wikipedia.org/wiki/SPECTRE), il va falloir faire tes preuves. On ne prend pas n'importe qui nous !

Ta mission est très simple. Il va falloir que tu entres sur les serveurs du MI6. Oui, rien que ça. Et pour couronner ton oeuvre, tu vas entrer au MI6 en utilisant le login et mot de passe de James Bond, notre ennemi juré.

Nous avons écouté le traffic vers leurs serveurs: télécharge le fichier en bas de cet énoncé.

# Se connecter aux serveurs du MI6

Tu y accèdes sur [le web avec ton navigateur préféré](http://ADRESSE_IP_MACHINE:8080).

# Wireshark

Pour lire le fichier `net1.pcapng`, il te faut **Wireshark**.

Installe-le sur ta machine, ou demande à tes parents. Sous Ubuntu/Mint, c'est quelque chose comme `sudo apt-get install wireshark`.

Ensuite, lance `wireshark net1.pcapng`. 

Tu vas voir une interface graphique qui te montre les paquets réseau, c'est à dire l'information qui a circulé vers le MI6 et que SPECTRE a intercepté. Tu peux cliquer sur les lignes du haut, et obtenir plus de détails dans le bas. Regarde bien un peu chacune des lignes. Essaie de comprendre ce qui se passe, et surtout de récupérer le login (username) et le mot de passe de James Bond.

NB. Ceci ne serait pas possible avec *httpS*.
