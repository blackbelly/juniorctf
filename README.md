# Junior CTF

A CTF that looks real "like in movies", for children aged 8-16 (appromimately)

We don't expect them to know anything about Unix (yet) nor how to code (yet). But they'll learn :)

**They must be able to read, write, and count (additions, substractions, multiplications, divisions)**. 
And they need access to a computer (preferably Linux) :)

The CTF framework is based on [CTFd](https://ctfd.io). This repository is a collection of challenges which are suitable for teenagers.

## What does it look like?


![](https://framagit.org/axellec/juniorctf/uploads/c08add15338aa8d5c0bab0b9a6f2132b/junior-home.png)

See other [Screenshots](https://framagit.org/axellec/juniorctf/wikis/Screenshots)

Those challenges are provided for *educational use only*, to help kids understand computer security and become security aware. For the rest, let's state it briefly, **attackers are baaaaaad**.

## Install, contribute, docs

- How to [install](./docs/install.md)
- How to [contribute](./docs/contributing.md)
- Presentation of [Junior CTF at Hack.lu 2018](https://slides.com/invisibleman/juniorctf/fullscreen#)


## License

This is released under the [MIT license](./LICENSE). Feel free to share! (hmm, not to kids, right, because solutions are included in the repository :)


